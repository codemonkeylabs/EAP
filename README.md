# Extensible Authentication Protocol - EAP
This module provides types and definitions as well as on the wire encoding and decoding of EAP packets and messages as per RFC 3748
